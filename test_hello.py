import logging
import random

logger = logging.getLogger()



def test_hello():
    if random.random() < 0.5:
        logger.info('Test passed')
    else:
        assert False, 'Test failed'
